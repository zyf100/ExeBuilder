package exebuilder.data;

public class Description {

  public static String APP_TITLE = "ExeBuilder 1.0.4 Alpha";

  public static String DOWNLOAD_JDK = "JDK下载";


  public static String STEP_0_TITLE = "欢迎使用ExeBuilder！";
  public static String STEP_0_INFO = "ExeBuilder是一款基于JDK的打包工具，你可以使用它轻松的创建基于Java的Windows GUI应用程序和控制台应用程序。";

  public static String STEP_1_TITLE = "Step1.";
  public static String STEP_1_INFO = "若程序类型选择错误将导致生成的exe文件无法正常运行。应用程序名称将作为程序输出文件夹的名称，如果为空，则使用默认名称。";

  public static String STEP_2_TITLE = "Step2.";
  public static String STEP_2_INFO = "本程序依赖的JDK版本最低为JDK14。源文件目录下必须包含一个可执行Jar文件和项目依赖的lib文件夹（如果有）";

  public static String STEP_3_TITLE = "Step3.";
  public static String STEP_3_INFO = "生成exe文件的信息";

  public static String STEP_4_TITLE = "Step4.";
  public static String STEP_4_INFO = "生成项目";

}
